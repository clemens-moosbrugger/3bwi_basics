package at.clemens.remote;

public class Remote {
	private double weight;
	private double length;
	private double width;
	private double height;
	private Battery battery;
	
	public Remote(double w, double l, double wi, double he, Battery battery) {
		super();
		this.weight = w;
		this.length = l;
		this.width = wi;
		this.height = he;
		this.battery = battery;
	}

	public void turnOn() {
		System.out.println("I am turned on now");
	}

	public void turnOff() {
		System.out.println("I am turned off now");
	}

	public void sayHello() {
		System.out.println("Weight:" + this.weight);
	}



}
