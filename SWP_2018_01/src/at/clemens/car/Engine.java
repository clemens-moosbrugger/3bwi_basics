package at.clemens.car;

public class Engine {
	private boolean gas;
	private double power;
	
	public Engine(boolean gas, double power) {
		super();
		this.gas = gas;
		this.power = power;
	}

	public boolean isGas() {
		return gas;
	}

	public void setGas(boolean gas) {
		this.gas = gas;
	}
}
