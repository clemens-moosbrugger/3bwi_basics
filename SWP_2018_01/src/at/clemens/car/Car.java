package at.clemens.car;

public class Car {
	private String color;
	private double speed;
	private double BasePrice;
	private double BaseConsumption;
	private Producer Producer;
	private Engine Engine;
	private double price;
	private int kilometer;

	public Car(String color, double speed, double basePrice, double baseConsumption, Producer producer, Engine engine,
			int kilometer) {
		super();
		this.color = color;
		this.speed = speed;
		this.BasePrice = basePrice;
		this.BaseConsumption = baseConsumption;
		this.Producer = producer;
		this.Engine = engine;
		this.kilometer = kilometer;
		
	}

	public double getPrice() {
		double discount = Producer.getDiscount();
		double DiscountPrice = discount * BasePrice;
		return DiscountPrice;
	}

	public String getType() {
		Boolean EngineTypeBool = Engine.isGas();
		String EngineType;
		if (EngineTypeBool == false) {
			EngineType = "Benzin";
		} else {
			EngineType = "Diesel";
		}
		;

		return EngineType;

	}

	public String getColor() {
		return this.color;
	}

	public double getUsage() {
		double usage;
		if (kilometer <= 50000) {
			usage = BaseConsumption;
		} else {
			usage = BaseConsumption * 1.098;
		}
		;
		return usage;
	}

}
