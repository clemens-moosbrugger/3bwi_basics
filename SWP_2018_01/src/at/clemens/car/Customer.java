package at.clemens.car;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;

public class Customer {
	private String firstname;
	private String lastname;
	private int birhtday;
	
	private ArrayList<Car> cars;

	public Customer(String firstname, String lastname, int birthday) {
		super();
		this.firstname = firstname;
		this.birhtday = birthday;
		this.lastname = lastname;
		this.cars = new ArrayList<>();
	}

	public void addCar(Car c) {
		
		this.cars.add(c);
	}
	
	public String getCarColor() {
		String carColor = "";
		for(Car c : cars) {
			carColor = carColor + c.getColor() + "\n";
		}
		return carColor;
	}
//	public void getAge() {
		
//		LocalDate birthdate = new LocalDate(1970);      //Birth date
//		LocalDate now = new LocalDate();                        //Today's date
		 
//		Period period = new Period(birthdate, now, PeriodType.yearMonthDay());
		 
		//Now access the values as below
//		System.out.println(period.getDays());
//		System.out.println(period.getMonths());
//		System.out.println(period.getYears());

//	}

}
