package at.clemens.car;

public class Producer {
	private String name;
	private String country;
	public Producer(String name, String country, double discount) {
		super();
		this.name = name;
		this.country = country;
		this.discount = discount;
	}

	private double discount;

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
}

